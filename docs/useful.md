## Information for analyzers

### Requesting Monte Carlo (MC) samples

Detailed instructions of what is required to make an MC request can be found [here](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ExoticsMCRequestsHowTo). But in summary you are going to need:

- A twiki page containing validation plots (also preshown at a LUP meeting)
- Attach to this twiki a tarball of the log files from the Generate_tf command that produced the evgen
- A completed mc request spreadsheet document (obtained from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC16SpreadSheet)
- A tarball of the joboptions
- Open a ticket on ATLMCPROD with the above information attached, as described here.

### Derivations

The [LLP1](https://gitlab.cern.ch/atlas/athena/-/blob/master/PhysicsAnalysis/DerivationFramework/DerivationFrameworkLLP/python/LLP1.py) derivation has been developed to accommodate LUP analyses that cannot use DAOD_PHYS. 

### Analysis software

If you are starting a new analysis, we strongly suggest that you use an existing analysis framework that uses the CP::Algorithms. The current recommendations for analysis in Release 25 are:

- [easyjet](https://gitlab.cern.ch/easyjet/easyjet/): AthAnalysis-based framework
- [TopCPToolkit](https://topcptoolkit.docs.cern.ch/): EventLoop-based framework

### Trigger information

### Background estimation

Documentation on a common tool for ABCD background estimation is available [here](https://twiki.cern.ch/twiki/bin/view/Main/ABCDMethod).

### Lifetime reweighting

Documentation on the lifetime reweighting procedure is [here](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/LifetimeReweighting).

### Useful trainings
   * **Virtual Pipelines Training** ([details](https://indico.cern.ch/event/904759/timetable/))
   * **Virtual Docker Training** ([details](https://indico.cern.ch/event/934651/overview))
