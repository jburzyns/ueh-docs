# Theory information

## Theory-related talks in ATLAS meetings

This section lists the invited theory talks given in LUP meetings (or other ATLAS meetings if of interest for the LUP community). You can get in contact with the [LUP conveners](mailto:atlas-phys-exotics-llp-conveners@cern.ch) if you wish to propose a theory talk at a LUP meeting.

| Talk | Date | Title | Speaker | Slides |
|------|------|-------|---------|--------|
| Talk | 14 January 2016 | *Exotic Quarks in Twin Higgs Models* | Ennio Salvoni | [slides](https://indico.cern.ch/event/474236/contribution/1/attachments/1211598/1767302/Talk_ATLAS_UEH.pdf) |
| Talk | 17 September 2015 | *Discovering Inelastic Thermal-Relic Dark Matter at Colliders* | Eder Izaguirre | [slides](https://indico.cern.ch/event/445699/contribution/1/attachments/1156316/1662296/iDMGroupTalk.pdf) |
| Talk | 18 June 2015 | *Lepton Number Violation in Higgs Decay (with displaced vertices)* | Fabrizio Nesti | [slides](https://indico.cern.ch/event/401933/contribution/2/attachments/805220/1103545/Cern_Nesti_LNVH.pdf) |
| Exotics seminar | 11 November 2014 | *Some Theoretical Motivations for Long-Lived Particle Searches* | Matt Strassler | [slides](https://indico.cern.ch/event/350354/contribution/0/material/slides/0.pdf) |
| Talk | 6 November 2014 | *Phenomenology of photon-jets* | Jakub Scholtz | [slides](https://indico.cern.ch/event/349822/session/0/contribution/4/material/slides/0.pdf) |
| Talk | 3 October 2014 | *Motivation for displaced signatures*, at the kick-off meeting on displaced vertices and lepton-jets | Josh Ruderman | [slides](https://indico.cern.ch/event/340723/contribution/2/material/slides/0.pdf) |
| Talk (kick-off meeting) | 3 October 2014 | *Illuminating dark sectors with dark photons* | David Curtin | [slides](https://indico.cern.ch/event/340723/contribution/3/material/slides/0.pdf) |
| Talk | 3 October 2014 | *Emerging jets*, at the kick-off meeting on displaced vertices and lepton-jets | Daniel Stolarski | [slides](https://indico.cern.ch/event/340723/contribution/4/material/slides/0.pdf) |
| Talk | 2 October 2014 | *Exotic tracking*, at the kick-off meeting on exotic tracking | Patrick Meade | [slides](https://indico.cern.ch/event/340504/contribution/3/material/slides/0.pdf) |
| Talk | 27 June 2014 | *Phenomenology of a light scalar mixing with the SM Higgs* | Jackson Clark | [slides](https://indico.cern.ch/event/325861/contribution/0/0/material/slides/0.pdf) |
| Talk | 5 June 2014 | *Exotic tracking* | Patrick Meade | [slides](https://indico.cern.ch/event/322197/contribution/0/material/slides/0.pdf) |
| Talk | 13 March 2014 | *Emerging jets from a dark sector* | Pedro Schwaller | [slides](https://indico.cern.ch/event/306866/contribution/0/material/slides/0.pdf) |

## Interesting papers/models

This section contains a list of papers where model or signatures of potential interest are described. There's a vast literature and no attempt here is made to have a comprehensive list of papers. Feel free to add more material if you judge it as interesting for the subgroup.

| Model | Title | arXiv |
|---------------|------------------------------------------------|--------------------------------------------------|
| Hidden Valley | Echoes of a hidden valley at hadron colliders | [arXiv 0604261](http://arxiv.org/abs/hep-ph/0604261) |
| Hidden Valley | Discovering the Higgs through highly-displaced vertices | [arXiv 0605193](http://arxiv.org/abs/hep-ph/0605193) |
| Hidden Valley | Possible effects of a hidden valley on a supersymmetric phenomenology | [arXiv 0607160](http://arxiv.org/abs/hep-ph/0607160) |
| Stealth Supersymmetry | Stealth supersymmetry | [arXiv 1105.5135](http://arxiv.org/abs/arXiv:1105.5135) |
| Stealth Supersymmetry | A stealth supersymmetry sampler | [arXiv 1201.4875](http://arxiv.org/abs/arXiv:1201.4875) |
| Invisible Higgs | Looking for an Invisible Higgs Signal at the LHC | [arXiv 1211.7015](http://arxiv.org/abs/arXiv:1211.7015) |
| Invisible Higgs | Status of invisible Higgs decays | [arXiv 1302.5694](http://arxiv.org/abs/arXiv:1302.5694) |
| Exotic Higgs | Exotic Decays of the 125 GeV Higgs Boson | [arXiv 1312.4992](http://arxiv.org/abs/arXiv:1312.4992) |
| Exotic Higgs | Exotic Higgs decays in the golden channel | [arXiv 1405.1095](http://arxiv.org/abs/arXiv:1405.1095) |
| Exotic Higgs | Supersymmetric Exotic Decays of the 125 GeV Higgs Boson | [arXiv 1309.6633](http://arxiv.org/abs/arXiv:1309.6633) |
| Hidden sector | Implications of LHC searches for Higgs--portal dark matter | [arXiv 1112.3299](http://arxiv.org/abs/arXiv:1112.3299) |
| Hidden sector | Higgs boson decays to four fermions through an abelian hidden sector | [arXiv 0801.3456](http://arxiv.org/abs/arXiv:0801.3456) |
| Other models related to displaced vertices | Emerging jets | [arXiv 1502.05409](http://arxiv.org/abs/1502.05409) |
| Other models related to displaced vertices | Discovering Inelastic Thermal-Relic Dark Matter at Colliders | [arXiv 1508.03050](http://arxiv.org/abs/1508.03050) |
| Other models related to displaced vertices | Heavy neutrino searches at the LHC with displaced vertices | [arXiv 1312.2900](http://arxiv.org/abs/arXiv:1312.2900) |
| Other models related to displaced vertices | Probing Baryogenesis with Displaced Vertices at the LHC | [arXiv 1409.6729](http://arxiv.org/abs/arXiv:1409.6729) |
| Other models related to displaced vertices | Displaced Dark Matter at Colliders | [arXiv 0906.5013](http://arxiv.org/abs/arXiv:0906.5013) |
| Other models requiring unconventional approaches | Photon-jets | [arXiv 1210.3657](http://arxiv.org/abs/1210.3657) |
| Other models requiring unconventional approaches | Odd Tracks at Hadron Colliders | [arXiv 1103.3016](http://arxiv.org/abs/arXiv:1103.3016) |
