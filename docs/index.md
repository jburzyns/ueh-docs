# ATLAS Long-lived and Unconventional Physics (LUP)

Welcome to the ATLAS LUP subgroup page! This replaces the old [UEH Twiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/LongLivedParticlesSubgroup).

???+ warning "Work in progress!"

    Note: This site is under construction and may contain incomplete/incorrect information

## General Information

We specialize in searching for particles and signatures that require significant customization and the invention of new ATLAS techniques. 

### Conveners

- [Cristiano Sebastiani](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=11527)
- [Jackson Burzynski](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=11736)

to be reached via <atlas-exot-ueh-conveners@cern.ch>

### E-groups & Mailing List

The group mailing list is: <hn-atlas-exotics-llp@cern.ch> [\[join\]](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=hn-atlas-exotics-llp) [\[archive\]](https://groups.cern.ch/group/hn-atlas-exotics-llp)

### Meetings
LUP meetings take place every Thursday at 16h30 CERN time [[indico]](https://indico.cern.ch/category/3286/).

Overviews of all subgroup meetings can be found in the left side-bar.

### Mattermost
You can join our Mattermost team [here](https://mattermost.web.cern.ch/signup_user_complete/?id=ikui5dx1wpggt8pi3wgtk9wf6h&md=link&sbr=su).

## Contributing

The documentation source is hosted on GitLab in the 
[lup-docs](https://gitlab.cern.ch/jburzyns/lup-docs) 
repository.

[ccd]: https://how-to.docs.cern.ch
[ccd-mkdocs]: https://how-to.docs.cern.ch/gitlabpagessite/create_site/creategitlabrepo/create_with_mkdocs/

We encourage anyone to contribute to these documentation pages by opening a merge request there.
To serve the docs locally, you can clone the repo and use `mkdocs` see your changes.

```bash
git clone ssh://git@gitlab.cern.ch:7999/jburzyns/lup-docs.git
cd lup-docs
python -m pip install -r requirements
mkdocs serve
```

You will see a link to open a locally hosted version of the documentation pages in a browser.
These will auto-update with changes to the source files.

## Forking this Page

To host your own version of this page at CERN, see CERN's [how-to.docs.cern.ch][ccd], specifically [the page on how to set up a MkDocs instance][ccd-mkdocs].

## History
Past Exotics LUP/UEH sub-conveners:

- Margaret Lutz (April 2022 - March 2024)
- Louie Corpe (Oct 2021 - Sept 2023)
- Matthias Danniger (April 2020 - March 2022)
- Cristiano Alpigiani (Oct 2019 - Sept 2021)
- John Stupak ()
- Emma Torro ()
- Monica Verducci ()
- Stefano Giagu (Oct 2015 - Sep 2016)
- James Beacham (Apr 2015 - Mar 2016)
- Benjamin Brau (Aug 2014 - Sep 2015)
- Andrea Coccaro (Apr 2014 - Mar 2015)
- Christopher Marino (Apr 2014 - Jul 2014)
- Daniel Ventura (Oct 2012 - Mar 2014)
- Gordon Watts (Oct 2012 - Sep 2013)
- Philippe Mermod (Oct 2011 - Sep 2012)
- Stefano Giagu (Oct 2010 - Sep 2011)
