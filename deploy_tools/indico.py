import argparse
import datetime
import logging
import multiprocessing as mp
import os
import urllib
from dataclasses import dataclass
from pathlib import Path

import requests
import yaml
from mkdocs_md import Markdown

logging.basicConfig(level=logging.INFO)


class BearerAuth(requests.auth.AuthBase):
    def __init__(self, token: str):
        self.token = token

    def __call__(self, req):
        req.headers["authorization"] = "Bearer " + self.token
        return req


@dataclass
class Contribution:
    title: str
    url: str
    speakers: list[dict] | str
    minutes: str
    start_time: str = None  # HH:MM:SS

    def __post_init__(self):
        self.speakers = ", ".join([f"{s['first_name']} {s['last_name']}" for s in self.speakers])
        self.start_time = datetime.datetime.strptime(self.start_time, "%H:%M:%S")

    def __str__(self):
        return f" - [{self.title}]({self.url}) - {self.speakers}"

    def __lt__(self, other):
        return self.start_time < other.start_time


@dataclass
class Event:
    url: str
    date: str
    title: str
    contributions: list[dict] | list[Contribution]

    def __post_init__(self):
        self.contributions = sorted(
            [
                Contribution(c["title"], c["url"], c["speakers"], c["note"], c["startDate"]["time"])
                for c in reversed(self.contributions)
            ]
        )

    def __lt__(self, other):
        return self.date > other.date

    def __str__(self):
        md = Markdown()
        date = md.link(self.date, self.url)
        md += md.header(f"{date} - {self.title}", level=4)
        md += "\n".join([str(c) for c in self.contributions])
        return str(md)

    def minutes(self):
        md = Markdown()
        date = md.link(self.date, self.url)
        md += md.header(f"{date} - {self.title}", level=4)
        for c in self.contributions:
            if c.minutes:
                md += md.admonition("note", str(c)[3:], str(c.minutes["html"]))
        return str(md)


@dataclass
class Meetings:
    meetings: list[Event]
    url: str
    start_date: str

    def __post_init__(self):
        self.meetings = list(sorted(self.meetings))

    def __add__(self, other):
        return Meetings(self.meetings + other.meetings, self.url, self.start_date)

    def meeting_list_md(self, name: str, when: str, target: Path):
        target = Path(str(target).replace("docs/", "indicomb_copy/"))
        target.parent.mkdir(parents=True, exist_ok=True)
        md = Markdown()
        md += md.header(f"{name} Meetings", level=1)
        name = md.link(name, self.url)
        md += f"The {name} meetings take place every {when} CERN time."
        md += "\n\nYou can find a meeting overview below.\n\n"
        md += md.header("Meeting Overview", level=2)
        md += f"*last updated: {datetime.datetime.now().strftime('%Y-%m-%d %H:%M')}*\n"
        md += "\n\n".join([str(m) for m in self.meetings])
        md += "\n\n"
        md += f"*date cutoff: {self.start_date}, for older meetings please check [indico]({self.url})*\n"
        print(str(md))
        with open(target, "w") as f:
            f.write(str(md))

    def minutes_md(self, name: str, target: Path):
        target = Path(str(target).replace("docs/", "indicomb_copy/"))
        target.parent.mkdir(parents=True, exist_ok=True)
        md = Markdown()
        md += md.header(f"{name.capitalize()} Meetings Minutes", level=1)
        md += f"*last updated: {datetime.datetime.now().strftime('%Y-%m-%d %H:%M')}*\n"
        md += "\n\n".join([m.minutes() for m in self.meetings])
        md += "\n\n"
        md += f"*date cutoff: {self.start_date}, for older meetings please check [indico]({self.url})*\n"
        with open(target, "w") as f:
            f.write(str(md))

    def add_topic(
        self,
        target: Path,
        include: list[str] | None = None,
        exclude: list[str] | None = None,
    ):
        if include is None:
            include = []
        if exclude is None:
            exclude = []
        data = {"Date": [], "Title": [], "Speakers": []}
        for e in self.meetings:
            for c in e.contributions:
                if any(x.lower() in c.title.lower() for x in include) and not any(
                    x.lower() in c.title.lower() for x in exclude
                ):
                    data["Date"].append(f"[{e.date}]({c.url})")
                    data["Title"].append(c.title)
                    data["Speakers"].append(c.speakers)

        md = Markdown()
        md += "\n"
        md += md.header("Meeting Contributions", level=2)
        md += md.table(data)
        md += "\n\n"
        md += f"*search terms: {include}*\n"
        md += f"*date cutoff: {self.start_date}, for older meetings please check [indico]({self.url})*\n"
        target = Path(str(target).replace("docs/", "indicomb_append/"))
        target.parent.mkdir(parents=True, exist_ok=True)
        with open(target, "w") as f:
            f.write(str(md))


@dataclass
class Indico:
    category_id: int
    api_token: str = None
    site: str = "https://indico.cern.ch"
    years: int = 4

    def __post_init__(self):
        if self.api_token is None:
            self.api_token = os.environ.get("INDICO_API_TOKEN", os.environ.get("INDICO_API_KEY", None))
        if self.api_token is None:
            raise ValueError("Please provide an API token or set INDICO_API_TOKEN or INDICO_API_KEY in your env")

        self.url = f"{self.site}/category/{self.category_id}/"
        today = datetime.date.today()
        start_date = today - datetime.timedelta(days=365 * self.years)
        self.start_date = start_date.strftime("%Y-%m-%d")
        logging.info(f"Feteching meetings for {self.category_id} from {self.start_date}")
        self.events = self.get_category(category_id=self.category_id, start_date=self.start_date)

    def request(self, path: str, params: dict):
        items = list(params.items()) if hasattr(params, "items") else list(params)
        items = sorted(items, key=lambda x: x[0].lower())
        url = f"{self.site}{path}?{urllib.parse.urlencode(items)}"
        response = requests.get(url, auth=BearerAuth(self.api_token))
        response.encoding = response.apparent_encoding
        return response.json()

    def get_category(self, category_id: int, start_date: str, end_date: str = "+15d"):
        category_params = {"from": start_date, "to": end_date, "order": "start"}
        category_path = f"/export/categ/{category_id}.json"
        return self.request(category_path, params=category_params)["results"]

    def filter_events(self, results: dict, include: list[str] = None, exclude: list[str] = None):
        if include is None:
            include = []
        if exclude is None:
            exclude = []
        include = [x.lower() for x in include]
        exclude = [x.lower() for x in exclude]
        results = [
            r
            for r in results
            if any(x in r["title"].lower() for x in include) and not any(x in r["title"].lower() for x in exclude)
        ]
        return results

    def process_event(self, event: dict, params: dict):
        event_path = f"/export/event/{event['id']}.json"
        event = self.request(event_path, params=params)["results"]
        assert len(event) == 1
        event = event[0]
        return Event(
            url=event["url"],
            date=event["startDate"]["date"],
            title=event["title"],
            contributions=event["contributions"],
        )

    def get_meetings(self, include: list[str] = None, exclude: list[str] = None) -> Meetings:
        if exclude is None:
            exclude = []
        exclude += ["cancelled"]
        events = self.filter_events(self.events, include=include, exclude=exclude)
        params = {"detail": "contributions"}
        results = []
        pool = mp.Pool(processes=mp.cpu_count())
        for event in events:
            kwargs = {"event": event, "params": params}
            pool.apply_async(self.process_event, kwds=kwargs, callback=results.append)
        pool.close()
        pool.join()
        return Meetings(list(sorted(results)), self.url, self.start_date)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", required=True, type=str, help="Path to the config file")
    args = parser.parse_args()

    # load config
    docs_dir = Path(__file__).parent.parent
    assert (docs_dir / "docs").exists(), f"docs directory does not exist in {docs_dir}"
    with open(args.config) as f:
        config = yaml.safe_load(f)

    # iniitialise indico

    # get sub configs
    summary_config = config["meeting_summaries"]

    # get the meetings by category
    categorised = {}
    for name, config in summary_config.items():
        ind = Indico(category_id=config["category_id"], years=config["years"])
        logging.info(f"Getting meetings for {name}")
        categorised[name] = ind.get_meetings(include=config["include"])

    # create meeting overview pages
    for name, config in summary_config.items():
        logging.info(f"Creating meeting overview for {name}")
        categorised[name].meeting_list_md(name=name, when=config["when"], target=config["target"])

if __name__ == "__main__":
    main()
