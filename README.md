# Documentation of the Exotics LUP subgroup

This repository serves as documentation for the ATLAS Exotics LUP (previously UEH) subgroup.

The docs are hosted here

- https://atlas-exot-lup.docs.cern.ch/

If you want to modify the documentation, please submit a merge request.
